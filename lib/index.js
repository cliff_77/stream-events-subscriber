/*global module:true, require:true */
/*jslint white:true, unparam:true, nomen:true */

'use strict';

var rabbitHub = require('rabbitmq-nodejs-client');
var events = require('events');
var path = require('path');

var envHost = process.env.RABBITMQ_HOST || 'slc-mobiledev.dev-cms.com';

var StreamEventsSubscriber = function()
{
  this.station = this.logger = this.subHub = this.hub = null;
};

StreamEventsSubscriber.prototype = new events.EventEmitter();

StreamEventsSubscriber.prototype.init = function(station, logger)
{
  var self = this;
  this.station = station;
  this.logger = logger;
  
  if (!envHost)
  {
      this.emit('error', "No RABBITMQ_HOST defined in environment.");
      /* istanbul ignore if */
      if (this.logger)
      {
          this.logger.debug('StreamEventsPublisher failed to initialize.');
      }
      return;
  }
  
  this.subHub = rabbitHub.create( { host: envHost, task: 'sub', channel: 'events' } );
  this.subHub.once('connection', function(hub)
  {
      this.hub = hub;
      this.emit('connect', hub);
      /* istanbul ignore if */
      if (this.logger)
      {
          this.logger.debug('StreamEventsSubscriber RabbitMQ client connection established');
      }
      
      hub.on('message', function(message)
      {
          this.emit('message', message);
      }.bind(this));

  }.bind(this));
  this.subHub.on('error', function( err )
  {
      this.emit('error', err);
      /* istanbul ignore if */
      if (this.logger)
      {
          this.logger.debug('StreamEventsSubscriber RabbitMQ client error: ', err);
      }
  }.bind(this));
  
  this.subHub.connect();
};

module.exports = StreamEventsSubscriber;
