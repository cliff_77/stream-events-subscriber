/*global describe:true, it:true, module:true, require:true */
/*jslint white:true, unparam:true, nomen:true */
/*jshint expr: true*/

'use strict';

var should = require('should');
var rewire = require('rewire');
var path = require('path');
var rabbitMQMock = require(path.join(__dirname, 'mocks', 'rabbitmq'));
var rabbitMQErrorMock = require(path.join(__dirname, 'mocks', 'rabbitmq-error'));
var rabbitHub = require('rabbitmq-nodejs-client');

function createStreamEventsSubscriber( envHost, rabbitMQ )
{
    var StreamEventsSubscriber = rewire('../lib/index.js');
    StreamEventsSubscriber.__set__('envHost', envHost);
    StreamEventsSubscriber.__set__('rabbitHub', rabbitMQ);
    return new StreamEventsSubscriber();
}

describe('stream-events-subscriber', function()
{
    var rabbitMQFakeHost = 'fakehost';
    describe('#init', function()
    {
      
        it('should initialize', function( done )
        {
            var sub = createStreamEventsSubscriber(rabbitMQFakeHost, rabbitMQMock);
            sub.on('connect', function( hub )
            {
                done();
            });
    
            sub.on('error', function( err )
            {
                throw new Error('Failed To Initialize');
            });
            sub.init({});
        });
        
        it('should fail to initialize with no envHost', function( done )
        {
            var sub = createStreamEventsSubscriber('', rabbitMQErrorMock);
            sub.on('connect', function( hub )
            {
                throw new Error('Should not initialize');
            });
          
            sub.on('error', function( err )
            {
                done();
            });
          
            sub.init({});
        });
        
        it('should fail to initialize with rabbitMQ connect error', function( done )
        {
            var sub = createStreamEventsSubscriber(rabbitMQFakeHost, rabbitMQErrorMock);
            sub.on('connect', function( hub )
            {
                throw new Error('Should not initialize');
            });
            
            sub.on('error', function( err )
            {
                done();
            });
            
            sub.init({});
        });
    });
    describe('#receive message', function()
    {
        it('should receive a message', function( done )
        {
            var testMessage = 'testing 1 2 3';
            var sub = createStreamEventsSubscriber(rabbitMQFakeHost, rabbitMQMock);
            
            sub.on('message', function( message )
            {
                message.should.be.a.String.and.equal(testMessage);
                done();
            });
            
            sub.on('connect', function( messageHub )
            {
                setTimeout(function()
                {
                  this.emit('message', testMessage);
                }.bind(messageHub), 0);
            });
                
            sub.on('error', function( err )
            {
                throw new Error('Should not throw an error');
            });
            
            sub.init({});
        });
    });
});