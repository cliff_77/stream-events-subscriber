var events = require('events');

var RabbitMQErrorMock = function()
{};

RabbitMQErrorMock.prototype = new events.EventEmitter;
RabbitMQErrorMock.prototype.connect = function(){
  setTimeout(function()
  {
    this.emit('error', new Error('Error'));
  }.bind(this), 0);
}


RabbitMQErrorMock.prototype.connected = false;
exports.create = function( options )
{
    var client = new RabbitMQErrorMock();
    
    /*
    setTimeout(function()
    {
        client.emit('error', new Error('Error'));
    }, 0);
    */
    
    return client;
};
