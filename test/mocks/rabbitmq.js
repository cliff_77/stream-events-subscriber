var events = require('events');


var RabbitMQHubMock = function() {}
RabbitMQHubMock.prototype = new events.EventEmitter();
RabbitMQHubMock.prototype.send = function(data) {}

var RabbitMQMock = function()
{};

exports.getReturnError = null;
exports.getReturnValue = '';

RabbitMQMock.prototype = new events.EventEmitter;
RabbitMQMock.prototype.connect = function() 
{
  setTimeout(function() 
  {
    var hub = new RabbitMQHubMock();
    this.emit('connection', hub);
  }.bind(this), 0);
}
exports.create = function( options )
{
    return new RabbitMQMock();
};